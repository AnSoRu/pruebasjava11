package com.pruebas11.model.article;

import java.util.Arrays;

public class Article {

    private float score;
    private String journal;
    private String article_type;
    private String title_display;
    private String [] author_display;
    private String publication_date;
    private String eissn;
    private String id;
    //private String [] abstract;


    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public String getArticle_type() {
        return article_type;
    }

    public void setArticle_type(String article_type) {
        this.article_type = article_type;
    }

    public String getTitle_display() {
        return title_display;
    }

    public void setTitle_display(String title_display) {
        this.title_display = title_display;
    }

    public String[] getAuthor_display() {
        return author_display;
    }

    public void setAuthor_display(String[] author_display) {
        this.author_display = author_display;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        this.publication_date = publication_date;
    }

    public String getEissn() {
        return eissn;
    }

    public void setEissn(String eissn) {
        this.eissn = eissn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Article{" +
                "score=" + score +
                ", journal='" + journal + '\'' +
                ", article_type='" + article_type + '\'' +
                ", title_display='" + title_display + '\'' +
                ", author_display=" + Arrays.toString(author_display) +
                ", publication_date='" + publication_date + '\'' +
                ", eissn='" + eissn + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
