package main;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pruebas11.model.article.Article;
import flexjson.JSONDeserializer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


public class HTTPRequestAndBeautifyJsonResponse {

    public static String beautify(String json) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(json,Object.class);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
    }

    public static void main(String[] args) {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://api.plos.org/search?q=title:Coronavirus"))
                .GET()
                .build();

        HttpClient client = HttpClient.newHttpClient();
        HttpResponse response = null;
        try{
            response = client.send(request,HttpResponse.BodyHandlers.ofString());
        }catch (Exception e){
            e.printStackTrace();
        }

        JSONParser parser = new JSONParser();
        try {

            JSONObject jsonObject = (JSONObject) parser.parse(response.body().toString());
            JSONObject responseKey = (JSONObject) jsonObject.get("response");
            JSONArray docs = (JSONArray) responseKey.get("docs");
            JSONObject [] articles = new JSONObject[docs.size()];

            for(int i = 0; i < docs.size(); i++){
                articles[i] = (JSONObject) docs.get(i);
            }

            for (JSONObject article : articles) {
                article.remove("abstract");
                //Article article1 = new JSONDeserializer<Article>().deserialize(article.toString(),Article.class);
                System.out.println(HTTPRequestAndBeautifyJsonResponse.beautify(article.toString()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*User user = new JSONDeserializer<User>().deserialize(response.body().toString());

        System.out.println("User userID: " + user.getUserId());
        System.out.println("User ID: " + user.getId());
        System.out.println("User title: " + user.getTitle());
        System.out.println("User completed: " + user.isCompleted());*/

    }
}
