package main;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class EncryptDecrypUtil {


    public static void main(String[] args) {

        String input = "baeldung";
        SecretKey key = null;
        try {
            key = AESUtil.generateKey(128);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        IvParameterSpec ivParameterSpec = AESUtil.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";
        String cipherText = null;
        try {
            cipherText = AESUtil.encrypt(algorithm, input, key, ivParameterSpec);
            System.out.println("Texto cifrado");
            System.out.println(cipherText);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | BadPaddingException | InvalidKeyException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        try {
            String plainText = AESUtil.decrypt(algorithm, cipherText, key, ivParameterSpec);
            System.out.println("Texto descifrado");
            System.out.println(plainText);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }
}
